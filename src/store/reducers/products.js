import * as actionTypes from '../actions/actionTypes';

const initialState = {
    products: [],
    isLoading: false,
    error: null,
    product: {},
    totalCount: 0,
    rowsPerPage: 5,
    currentPage: 1,
    pages: []
}


const initRowsPerPage = (state, action) => {
    return { ...state }
}

const changeRowsPerPage = (state, action) => {
    return {
        ...state,
        rowsPerPage: action.rowsPerPage,
        currentPage: 1
    }
}

const updatePages = (state, action) => {
    return {
        ...state,
        pages: action.pages
    }
}

const changePage = (state, action) => {
    return {
        ...state,
        currentPage: action.currentPage
    }
}

const fetchProductsStart = (state, action) => {
    return {
        ...state,
        isLoading: true
    }
}

const fetchProductsSuccess = (state, action) => {
    return {
        ...state,
        products: action.products,
        totalCount: action.totalCount,
        isLoading: false
    }
}

const fetchProductsFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const createProductStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null
    }
}

const createProductSuccess = (state, action) => {
    return {
        ...state,
        isLoading: false,
        product: action.createdProduct,
        error: null
    }
}

const createProductFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const deleteProductStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null
    }
}

const deleteProductFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const deleteProductSuccess = (state, action) => {
    return {
        ...state,
        product: action.deletedProduct,
        isLoading: false
    }
}


const editProductStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null
    }
}

const editProductSuccess = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: null,
        product: action.product
    }
}

const editProductFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const updateProductStart = (state, action) => {
    return {
        ...state,
        isLoading: true,
        error: null
    }
}

const updateProductSuccess = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: null
    }
}

const updateProductFailed = (state, action) => {
    return {
        ...state,
        isLoading: false,
        error: action.error
    }
}

const products = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.INIT_ROWS_PER_PAGE: return initRowsPerPage(state, action);
        case actionTypes.CHANGE_ROWS_PER_PAGE: return changeRowsPerPage(state, action);
        case actionTypes.UPDATE_PAGES: return updatePages(state, action);
        case actionTypes.CHANGE_PAGE: return changePage(state, action);

        case actionTypes.FETCH_PRODUCTS_START: return fetchProductsStart(state, action);
        case actionTypes.FETCH_PRODUCTS_SUCCESS: return fetchProductsSuccess(state, action);
        case actionTypes.FETCH_PRODUCTS_FAILED: return fetchProductsFailed(state, action);

        case actionTypes.CREATE_PRODUCT_START: return createProductStart(state, action);
        case actionTypes.CREATE_PRODUCT_SUCCESS: return createProductSuccess(state, action);
        case actionTypes.CREATE_PRODUCT_FAILED: return createProductFailed(state, action);

        case actionTypes.DELETE_PRODUCT_START: return deleteProductStart(state, action);
        case actionTypes.DELETE_PRODUCT_SUCCESS: return deleteProductSuccess(state, action);
        case actionTypes.DELETE_PRODUCT_FAILED: return deleteProductFailed(state, action);
        
        case actionTypes.EDIT_PRODUCT_START: return editProductStart(state, action);
        case actionTypes.EDIT_PRODUCT_SUCCESS: return editProductSuccess(state, action);
        case actionTypes.EDIT_PRODUCT_FAILED: return editProductFailed(state, action);

        case actionTypes.UPDATE_PRODUCT_START: return updateProductStart(state, action);
        case actionTypes.UPDATE_PRODUCT_SUCCESS: return updateProductSuccess(state, action);
        case actionTypes.UPDATE_PRODUCT_FAILED: return updateProductFailed(state, action);

        default: return state;
    }
}

export default products;