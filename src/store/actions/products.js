import http from '../../services/http';
import * as actionTypes from './actionTypes';
import _ from 'lodash'

const computePages = (totalCount, rowsPerPage) => {
    const totalPages = Math.ceil(totalCount / rowsPerPage);
    const pages = _.range(1, totalPages + 1);
    return pages
}

export const initRowsPerPage = () => dispatch => {
   dispatch({
        type: actionTypes.INIT_ROWS_PER_PAGE
   })
}


export const changeRowsPerPage = (value) => {
    return {
        type: actionTypes.CHANGE_ROWS_PER_PAGE,
        rowsPerPage: parseInt(value)
    }
}

export const updatePages = (totalCount, rowsPerPage) => {
    // console.log(totalCount, rowsPerPage)
    return {
        type: actionTypes.UPDATE_PAGES,
        pages: computePages(totalCount, rowsPerPage)
    }
}

export const changePage = pageNum => {
    return {
        type: actionTypes.CHANGE_PAGE,
        currentPage: pageNum
    }
}

// ------------- fetch products
export const fetchProductsStart = () => {
    return {
        type: actionTypes.FETCH_PRODUCTS_START
    }
}

export const fetchProductsSuccess = (data) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_SUCCESS,
        products: data.products,
        totalCount: data.totalCount
    }
}

export const fetchProductsFailed = (error) => {
    return {
        type: actionTypes.FETCH_PRODUCTS_FAILED,
        error
    }
}

export const fetchProducts = (rowsPerPage, currentPage) => async dispatch => {
    try {
        dispatch(fetchProductsStart());
        
        const skip = (currentPage - 1) * rowsPerPage;
        let response = await http.get(`/admin/products?limit=${rowsPerPage}&skip=${skip}`)

        // console.log('[store-action products.js] fetch products', response.data);
        dispatch(fetchProductsSuccess(response.data));

    } catch (err) {
        dispatch(fetchProductsFailed(err.response.data.error));
    }
}

// ------------- create products
export const createProductStart = () => {
    return {
        type: actionTypes.CREATE_PRODUCT_START
    }
}

export const createProductSuccess = (createdProduct) => {
    return {
        type: actionTypes.CREATE_PRODUCT_SUCCESS,
        product: createdProduct
    }
}

export const createProductFailed = (error) => {
    return {
        type: actionTypes.CREATE_PRODUCT_FAILED,
        error: error
    }
}

export const createProduct = productData => async dispatch => {
    try {
        // console.log('[redux action product.js] ', productData)
        dispatch(createProductStart());
        const response = await http.post('/admin/products', productData);
        dispatch(createProductSuccess(response.data))
    } catch (error) {
        dispatch(createProductFailed(error.response.data.error))
    }
}

// ------------- delete products
export const deleteProductStart = () => {
    return { type: actionTypes.DELETE_PRODUCT_START }
}

export const deleteProductSuccess = (deletedProduct) => {
    return {
        type: actionTypes.DELETE_PRODUCT_SUCCESS,
        product: deletedProduct
    }
}

export const deleteProductFailed = (error) => {
    return {
        type: actionTypes.DELETE_PRODUCT_FAILED,
        error: error
    }
}

export const deleteProduct = productId => async dispatch => {
    try {
        dispatch(deleteProductStart())
        const response = await http.delete(`/admin/products/${productId}`);
        dispatch(deleteProductSuccess(response.data));
    } catch (error) {
        // console.log(error.response);
        dispatch(deleteProductFailed(error.response.data.error))
    }
}

export const editProductStart = () => {
    return { type: actionTypes.EDIT_PRODUCT_START }
}

export const editProductSuccess = (product) => {
    return {
        type: actionTypes.EDIT_PRODUCT_SUCCESS,
        product
    }
}

export const editProductFailed = (error) => {
    return {
        type: actionTypes.EDIT_PRODUCT_FAILED,
        error
    }
}

export const editProduct = productId => async dispatch => {
    try {
        dispatch(editProductStart())
        const response = await http.get('/admin/products/' + productId)
        dispatch(editProductSuccess(response.data.product))

    } catch (err) {
        dispatch(editProductFailed(err.response.data.error))
    }
}


export const updateProductStart = () => {
    return { type: actionTypes.UPDATE_PRODUCT_START }
}

export const updateProductSuccess = (product) => {
    return {
        type: actionTypes.UPDATE_PRODUCT_SUCCESS,
        product
    }
}

export const updateProductFailed = (error) => {
    return {
        type: actionTypes.UPDATE_PRODUCT_FAILED,
        error
    }
}

export const updateProduct = (productId, formData) => async dispatch => {
    try {
        dispatch(updateProductStart())
        const response = await http.patch('/admin/products/' + productId, formData)
        // console.log('[products.js] update product ', response)
        dispatch(updateProductSuccess(response.data))

    } catch (err) {
        dispatch(updateProductFailed(err.response.data.error))
    }
}