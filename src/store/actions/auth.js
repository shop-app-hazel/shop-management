import * as actionTypes from './actionTypes'
import http from '../../services/http'
import jwt_decode from 'jwt-decode'

const decodeToken = (token) => {
    const decodedToken = jwt_decode(token)
    // console.log(decodedToken)
    return decodedToken
}


export const authCheckState = () => {
    return dispatch => {

        const token = localStorage.getItem('shop-mgmt-token');
        if (!token) {
            dispatch(logout());
        } else {
            const decodedToken = decodeToken(token)
            const expDate = new Date(decodedToken.exp * 1000);
            // console.log('token expiry ', expDate)

            if (expDate <= new Date()) {
                dispatch(logout());
            } else {
                const userId = decodedToken.userId;
                dispatch(authSuccess(token, userId));

                // console.log('check auth timeout', (expDate.getTime() - new Date().getTime())/1000 )
                dispatch(checkAuthTimeout((expDate.getTime() - new Date().getTime()) / 1000));
            }
        }
    }
}

export const authStart = () => {
    return { type: actionTypes.AUTH_START }
}

export const authSuccess = (token, userId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token,
        userId
    }
}

export const authFailed = (error) => {
    return {
        type: actionTypes.AUTH_FAILED,
        error
    }
}

export const checkAuthTimeout = (expTime) => {
    return dispatch => {
        setTimeout(() => {
            dispatch(logout());
        }, expTime * 1000);
    }
}

export const auth = (authData) => async dispatch => {
    try {
        dispatch(authStart())

        const response = await http.post('/admin/auth/login', authData)
        // console.log('[action auth.js response]' , response);

        dispatch(successAuthenticate(response.data))

    } catch (err) {
        // console.log('[action auth.js] ', err.response.data.error)
        dispatch(authFailed(err.response.data.error))
    }
}

const successAuthenticate = (data) => dispatch => {
    const decodedToken = decodeToken(data.token)

    localStorage.setItem('shop-mgmt-token', data.token);
    localStorage.setItem('shop-mgmt-userId', decodedToken.userId);

    dispatch(authSuccess(data.token, data.user._id))

    const expDate = new Date(decodedToken.exp * 1000);
    // console.log('check auth timeout', (expDate.getTime() - new Date().getTime())/1000 )
    dispatch(checkAuthTimeout((expDate.getTime() - new Date().getTime()) / 1000));
}

export const signUp = (signupData) => async dispatch => {
    try {
        dispatch(signUpStart())
        const response = await http.post('/admin/auth/signup', signupData)
        // console.log('register success ', response)
        dispatch(successAuthenticate(response.data))

        dispatch(signUpSuccess())

    } catch (err) {
        dispatch(signUpFailed(err.response.data.error))
    }
}

export const signUpStart = () => {
    return { type: actionTypes.SIGNUP_START }
}

export const signUpSuccess = () => {
    return { type: actionTypes.SIGNUP_SUCCESS }
}

export const signUpFailed = (error) => {
    return {
        type: actionTypes.SIGNUP_FAILED,
        error
    }
}

export const logout = () => {
    localStorage.removeItem('shop-mgmt-token');
    localStorage.removeItem('shop-mgmt-userId');
    return {
        type: actionTypes.AUTH_LOGOUT
    }
}