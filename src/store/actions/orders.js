import http from '../../services/http'
import * as actionTypes from './actionTypes'

export const fetchOrderStart = () => {
    return {
        type: actionTypes.FETCH_ORDERS_START,
        isLoading: true
    }
}

export const fetchOrderSuccess = (orders) => {
    return {
        type: actionTypes.FETCH_ORDERS_SUCCESS,
        isLoading: false,
        error: null,
        orders: orders
    }
}

export const fetchOrderFailed = error => {
    return {
        type: actionTypes.FETCH_ORDERS_FAILED,
        isLoading: false,
        error: error
    }
}

export const fetchOrders = () => async dispatch => {
    try {
        dispatch(fetchOrderStart())

        const response = await http.get('/orders')
        // console.log('[order.js actions] fetch orders by login user ', response)
        dispatch(fetchOrderSuccess(response.data.orders))
    } catch (err) {
        // console.log(err.response)
        dispatch(fetchOrderFailed(err.response.data.error))
    }
}