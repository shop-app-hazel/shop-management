export const units = [
    { value: 'kg', label: 'kg' },
    { value: 'ea', label: 'ea' }
]

export const pageItems = [
    { name: '5', value: '5' },
    { name: '10', value: '10' },
    { name: '25', value: '25' }
];