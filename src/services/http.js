import axios from 'axios';

const defaultOptions = {
    baseURL: process.env.REACT_APP_API_ENDPOINT,
    headers: {
      'Content-Type': 'application/json',
    },
  };

  // Create instance
  let instance = axios.create(defaultOptions);

  // Set the AUTH token for any request
  instance.interceptors.request.use(config => {
    const token = localStorage.getItem('shop-app-token');
    if(token) {
        config.headers.Authorization =  token ? `Bearer ${token}` : '';
    }
    return config;
  });

export default instance;