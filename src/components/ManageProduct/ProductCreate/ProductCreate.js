import React from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import classes from './ProductCreate.module.css';
import { checkValidation } from '../../../shared/utility';
import { units } from '../../../shared/selection';
import ProductCockpit from '../ProductCockpit/ProductCockpit';
import MaterialFormControl from '../../UI/MaterialFormControl/MaterialFormControl';
import MaterialSnackbar from '../../UI/MaterialSnackbar/MaterialSnackbar';
import { Grid, InputAdornment } from '@material-ui/core';


class ProductCreate extends React.Component {
    state = {
        error: null,
        isSnackbarOpen: false,
        isFormValid: false,
        productForm: {
            title: {
                label: 'Title',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            price: {
                label: 'Price',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'number',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false,
                adornment: {
                    start: <InputAdornment position="start">$</InputAdornment>
                }
            },

            unit: {
                label: 'Unit',
                elementType: 'select',
                helperText: 'Please select a unit for product',
                value: '',
                elementConfig: {
                    options: units
                },
                validationRules: {},
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            category: {
                label: 'Category',
                elementType: 'select',
                helperText: 'Please select your category',
                value: '',
                elementConfig: {
                    options: []
                },
                validationRules: {},
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            image: {
                label: 'Image',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            sku: {
                label: 'SKU',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            stock: {
                label: 'Stock',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'number',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },

            description: {
                label: 'Description',
                elementType: 'input',
                helperText: '',
                value: '',
                elementConfig: {
                    type: 'text',
                    placeholder: ''
                },
                validationRules: {
                    required: true
                },
                validation: {
                    isValid: false,
                    errorMessage: ''
                },
                touched: false
            },
        }
    }

    componentDidMount() {
        this.props.onFetchCategories();
    }

    componentDidUpdate(prevProps) {
        if (prevProps.categories !== this.props.categories) {
            let categories = [];
            categories.push({ value: 0, label: 'Please select' });
            this.props.categories.forEach(category => {
                categories.push({
                    value: category._id,
                    label: category.name
                })
            })

            const updatedForm = {
                ...this.state.productForm,
                category: {
                    ...this.state.productForm.category,
                    value: categories[0].value,
                    elementConfig: {
                        ...this.state.productForm.category.elementConfig,
                        options: categories
                    }
                }
            }
            this.setState({ productForm: updatedForm });
        }
    }

    inputChangedHandler = (e, formId) => {
        const updatedForm = {
            ...this.state.productForm,
            [formId]: {
                ...this.state.productForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.productForm[formId].validationRules),
                touched: true
            }
        }

        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ productForm: updatedForm, isFormValid })
    }

    closeSnackbarHandler = () => {
        this.setState({ isSnackbarOpen: false })
    }

    submitFormHandler = async (e) => {
        e.preventDefault();

        const formData = {};
        for (let formElement in this.state.productForm) {
            formData[formElement] = this.state.productForm[formElement].value;
        }
        // console.log('[ProductCreate.js] ', formData);

        await this.props.onCreateProduct(formData);

        if (!this.props.error) {
            this.props.history.push('/admin/products');
        } else {
            this.setState({ isSnackbarOpen: true })
            this.setState({ error: this.props.error })
        }
    }

    cancelChangesHandler = () => {
        this.props.history.push('/admin/products')
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.productForm) {
            formElementsArray.push({
                id: key,
                config: this.state.productForm[key]
            });
        }

        return (
            <div className="Container">
                {this.state.error
                    ? <MaterialSnackbar
                        vertical="bottom"
                        horizontal="left"
                        variant="error"
                        snackbarState={this.state.isSnackbarOpen}
                        onSnackbarClose={this.closeSnackbarHandler}
                        message={this.props.error} />
                    : null
                }
                <ProductCockpit
                    headerName="Create Product"
                    isFormValid={this.state.isFormValid}
                    isLoading={this.props.isLoading}
                    onSubmitForm={this.submitFormHandler}
                    onCancelForm={this.cancelChangesHandler} />

                <form onSubmit={this.submitFormHandler} className={classes.Form}>
                    <Grid container spacing={5}>
                        <Grid item xs={12} lg={4}>
                            <div className={classes.ImageContainer}>
                                {this.state.productForm.image.value
                                    ? <img style={{ height: '70%' }}
                                        alt="productImg"
                                        src={this.state.productForm.image.value} />
                                    : null}
                            </div>
                        </Grid>

                        <Grid item lg={1}></Grid>

                        <Grid item lg={6} xs={12}>
                            {formElementsArray.map(formElement => (
                                <Grid item xs={12} key={formElement.id}
                                    style={{ marginBottom: '1rem' }}>
                                    <MaterialFormControl
                                        id={formElement.id}
                                        label={formElement.config.label}
                                        value={formElement.config.value}
                                        helperText={formElement.config.helperText}
                                        elementType={formElement.config.elementType}
                                        elementConfig={formElement.config.elementConfig}
                                        isValid={formElement.config.validation.isValid}
                                        isTouched={formElement.config.touched}
                                        errorMessage={formElement.config.validation.errorMessage}
                                        onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                                        startAdornment={formElement.config.adornment
                                            ? formElement.config.adornment.start ? formElement.config.adornment.start : null
                                            : null}
                                        endAdornment={formElement.config.adornment
                                            ? formElement.config.adornment.end ? formElement.config.adornment.end : null
                                            : null} />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </form>
            </div >
        )
    }
}

const mapStateToProps = state => {
    return {
        categories: state.categories.categories,
        isLoading: state.products.isLoading,
        error: state.products.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchCategories: () => dispatch(actions.fetchCategories()),
        onCreateProduct: (product) => dispatch(actions.createProduct(product))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductCreate)
