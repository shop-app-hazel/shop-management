import React, { Fragment } from 'react';
import { connect } from 'react-redux';
import '../../../App.css';
import classes from './ProductEdit.module.css';
import * as actions from '../../../store/actions/index'
import Spinner from '../../UI/Spinner/Spinner'
import MaterialFormControl from '../../UI/MaterialFormControl/MaterialFormControl';
import MaterialSnackbar from '../../UI/MaterialSnackbar/MaterialSnackbar';
import ProductCockpit from '../ProductCockpit/ProductCockpit';
import { checkValidation } from '../../../shared/utility';
import { units } from '../../../shared/selection';
import { Grid, InputAdornment } from '@material-ui/core';


class ProductEdit extends React.Component {
    state = {
        error: null,
        isSnackbarOpen: false,
        isFormValid: false,
        productForm: {}
    }

    componentDidMount() {
        this.props.onFetchProduct(this.props.match.params.id);
        this.props.onFetchCategories();
    }

    componentDidUpdate(prevProps) {
        // if (prevProps.product !== this.props.product || prevProps.categories !== this.props.categories) {
        if (prevProps.product !== this.props.product) {
            // console.log('[ProductEdit.js componentDidUpdate] ', this.props.product)

            let productForm = {
                title: {
                    label: 'Title',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'text',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                price: {
                    label: 'Price',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'number',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false,
                    adornment: {
                        start: <InputAdornment position="start">$</InputAdornment>
                    }
                },

                unit: {
                    label: 'Unit',
                    elementType: 'select',
                    helperText: 'Please select a unit for product',
                    value: '',
                    elementConfig: {
                        options: units
                    },
                    validationRules: {},
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                category: {
                    label: 'Category',
                    elementType: 'select',
                    helperText: 'Please select your category',
                    value: '',
                    elementConfig: {
                        options: []
                    },
                    validationRules: {},
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                image: {
                    label: 'Image',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'text',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                sku: {
                    label: 'SKU',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'text',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                stock: {
                    label: 'Stock',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'number',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },

                description: {
                    label: 'Description',
                    elementType: 'input',
                    helperText: '',
                    value: '',
                    elementConfig: {
                        type: 'text',
                        placeholder: ''
                    },
                    validationRules: {
                        required: true
                    },
                    validation: {
                        isValid: false,
                        errorMessage: ''
                    },
                    touched: false
                },
            }

            const categoryArr = []
            this.props.categories.forEach(category => {
                categoryArr.push({
                    value: category._id,
                    label: category.name
                })
            })
            productForm.category.elementConfig.options = categoryArr

            for (let element in this.props.product) {
                for (let key in productForm) {
                    if (element === key) {
                        if (element === 'price') {
                            productForm[key].value = this.props.product[element].toFixed(2);
                        } else {
                            productForm[key].value = this.props.product[element];
                        }
                        productForm[key].validation = checkValidation(
                            this.props.product[element],
                            productForm[key].validationRules);
                        productForm[key].touched = true
                        break;
                    }
                }
            }
            // console.log('final product form ', productForm)

            let isFormValid = true;
            for (let element in productForm) {
                isFormValid = productForm[element].validation.isValid && isFormValid;
            }
            this.setState({ productForm, isFormValid })
        }
    }

    inputChangedHandler = (e, formId) => {
        // console.log('input change handler ', e)
        const updatedForm = {
            ...this.state.productForm,
            [formId]: {
                ...this.state.productForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.productForm[formId].validationRules),
                touched: true
            }
        }

        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ productForm: updatedForm, isFormValid })
    }

    cancelChangesHandler = () => {
        this.props.history.push('/admin/products')
    }

    submitFormHandler = async (e) => {
        e.preventDefault();

        const formData = {};
        for (let formElement in this.state.productForm) {
            formData[formElement] = this.state.productForm[formElement].value;
        }
        await this.props.onUpdateProduct(this.props.match.params.id, formData)

        if (!this.props.error) {
            this.props.history.push('/admin/products');
        } else {
            this.setState({ isSnackbarOpen: true })
            this.setState({ error: this.props.error })
        }
    }

    closeSnackbarHandler = () => {
        this.setState({ isSnackbarOpen: false })
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.productForm) {
            formElementsArray.push({
                id: key,
                config: this.state.productForm[key]
            });
        }

        let renderProduct = null;
        if (!this.props.product) {
            renderProduct = <Spinner />
        }

        renderProduct = (
            <div className="Container">
                {this.state.error
                    ? <MaterialSnackbar
                        vertical="bottom"
                        horizontal="left"
                        variant="error"
                        snackbarState={this.state.isSnackbarOpen}
                        onSnackbarClose={this.closeSnackbarHandler}
                        message={this.props.error} />
                    : null
                }

                <ProductCockpit
                    headerName="Edit Product"
                    isFormValid={this.state.isFormValid}
                    isLoading={this.props.isLoading}
                    onSubmitForm={this.submitFormHandler}
                    onCancelForm={this.cancelChangesHandler} />

                <form onSubmit={this.submitFormHandler} className={classes.Form}>
                    <Grid container spacing={5}>
                        <Grid item xs={12} lg={4}>
                            <div className={classes.ImageContainer}>
                                {this.state.productForm.image
                                    ? <img alt="productImg"
                                        style={{ height: '70%' }}
                                        src={this.state.productForm.image.value} />
                                    : null}
                            </div>
                        </Grid>

                        <Grid item lg={1}></Grid>

                        <Grid item lg={6} xs={12}>
                            {formElementsArray.map(formElement => (
                                <Grid item xs={12} key={formElement.id} style={{ marginBottom: '1rem' }}>
                                    <MaterialFormControl
                                        id={formElement.id}
                                        label={formElement.config.label}
                                        value={formElement.config.value}
                                        helperText={formElement.config.helperText}
                                        elementType={formElement.config.elementType}
                                        elementConfig={formElement.config.elementConfig}
                                        isValid={formElement.config.validation.isValid}
                                        isTouched={formElement.config.touched}
                                        errorMessage={formElement.config.validation.errorMessage}
                                        onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                                        startAdornment={formElement.config.adornment
                                            ? formElement.config.adornment.start ? formElement.config.adornment.start : null
                                            : null}
                                        endAdornment={formElement.config.adornment
                                            ? formElement.config.adornment.end ? formElement.config.adornment.end : null
                                            : null} />
                                </Grid>
                            ))}
                        </Grid>
                    </Grid>
                </form>
            </div>
        )

        return (
            <Fragment>
                {renderProduct}
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        product: state.products.product,
        categories: state.categories.categories,
        isLoading: state.products.isLoading,
        error: state.products.error
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onFetchProduct: (productId) => dispatch(actions.editProduct(productId)),
        onFetchCategories: () => dispatch(actions.fetchCategories()),
        onUpdateProduct: (productId, formData) => dispatch(actions.updateProduct(productId, formData))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductEdit)