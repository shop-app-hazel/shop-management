import React from 'react';
import MaterialDialog from '../../UI/MaterialDialog/MaterialDialog';

const ProductDelete = (props) => {
    return(
        <div>
            <MaterialDialog 
                open={props.open} 
                onClose={(value) => props.onClose(value)} 
                title="Delete Product"
                description={`Are you sure you want to delete this product: ${props.selectedProduct.title}`}
                actions="double" 
                actionNegative="NO" 
                actionPositive="YES"
            />
        </div>
    )
}

export default ProductDelete;