import React, { Fragment } from 'react';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/index';
import '../../App.css';
import TablePaging from '../../containers/Table/TablePaging/TablePaging';
import TablePaginate from '../../containers/Table/TablePaginate/TablePaginate';
import Spinner from '../UI/Spinner/Spinner'
import MaterialButton from '../UI/MaterialButton/MaterialButton';
import { pageItems } from '../../shared/selection';

import { EditOutlined, DeleteOutline } from '@material-ui/icons';
import {
    Table,
    TableHead,
    TableRow,
    TableCell,
    TableBody,
    CardMedia,
    Paper,
} from '@material-ui/core';


class ProductTable extends React.Component {

    changeRowsPerPageHandler = async (clickedItem) => {
        for (const i in pageItems) {
            if (pageItems[i].name === clickedItem) {
                await this.props.onUpdateRowsPerPage(pageItems[i].value)
                this.props.onUpdatePages(this.props.totalCount, this.props.rowsPerPage)
            }
        }
    }

    changePageHandler = pageNum => {
        this.props.onUpdateCurrentPage(pageNum)
    }

    render() {
        let renderTable = <Spinner />

        let renderProducts = (
            <Paper>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell width='10%'></TableCell>
                            <TableCell width='25%'>Product Name</TableCell>
                            <TableCell width='10%'>Price</TableCell>
                            <TableCell width='15%'>SKU</TableCell>
                            <TableCell width='10%'>Stock</TableCell>
                            <TableCell width='30%'>Actions</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {this.props.products.map(product => (
                            <TableRow key={product._id}>
                                <TableCell component="th" scope="row">
                                    <CardMedia component="img" height="50" width="50"
                                        src={product.image}
                                        style={{ objectFit: 'scale-down', width: 'auto' }}
                                    />
                                </TableCell>
                                <TableCell>{product.title}</TableCell>
                                <TableCell>${product.price.toFixed(2)}</TableCell>
                                <TableCell>{product.sku}</TableCell>
                                <TableCell>{product.stock}</TableCell>
                                <TableCell>
                                    <div className="Actions">
                                        <MaterialButton
                                            isDisabled={false}
                                            btnVariant="outlined"
                                            btnColor="primary"
                                            btnIcon={EditOutlined}
                                            btnName="Edit"
                                            onButtonClicked={() => { this.props.onEditProduct(product._id) }} />

                                        <MaterialButton
                                            isDisabled={false} btnVariant="outlined"
                                            btnColor="default" btnIcon={DeleteOutline}
                                            btnName="Delete"
                                            onButtonClicked={() => { this.props.onDeleteProduct(product) }} />
                                    </div>
                                </TableCell>
                            </TableRow>
                        ))}
                    </TableBody>
                </Table>
            </Paper>
        )

        if (!this.props.isLoading) {
            renderTable = (
                <Fragment>
                    <TablePaging
                        onChangeRowsPerPage={this.changeRowsPerPageHandler}
                        rowsPerPage={this.props.rowsPerPage}
                        totalCount={this.props.totalCount} />

                    {renderProducts}

                    <TablePaginate
                        onPageChange={this.changePageHandler}
                        rowsPerPage={this.props.rowsPerPage}
                        totalCount={this.props.totalCount}
                        pages={this.props.pages}
                        currentPage={this.props.currentPage} />
                </Fragment>
            )
        }
        return (
            <React.Fragment>
                {renderTable}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        isLoading: state.products.isLoading,
        rowsPerPage: state.products.rowsPerPage,
        totalCount: state.products.totalCount,
        currentPage: state.products.currentPage,
        pages: state.products.pages
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onUpdateRowsPerPage: (value) => dispatch(actions.changeRowsPerPage(value)),
        onUpdatePages: (totalCnt, rowsPerPage) => dispatch(actions.updatePages(totalCnt, rowsPerPage)),
        onUpdateCurrentPage: (pageNum) => dispatch(actions.changePage(pageNum))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ProductTable));