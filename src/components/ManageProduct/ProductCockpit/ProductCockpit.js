import React from 'react';
import { Link } from 'react-router-dom';
import "../../../App.css";
import classes from './ProductCockpit.module.css';
import MaterialButton from '../../UI/MaterialButton/MaterialButton';
import { IconButton } from '@material-ui/core';
import { ArrowBack, CancelRounded, SaveRounded } from '@material-ui/icons';

const ProductCockpit = props => {
    return (
        <div className="HeaderWrapper">
            <IconButton 
                style={{padding: '0', marginRight: '12px'}} 
                component={Link} to='/admin/products'>
                <ArrowBack style={{fontSize: '2.5rem'}} />
            </IconButton>
            <span className="HeaderTitle">
                {props.headerName}
            </span>

            <div className="Spacer"></div>

            <div className="Actions">
                <div className={classes.SubmitButtonContainer}>
                    <MaterialButton
                        onButtonClicked={props.onSubmitForm}
                        isDisabled={!props.isFormValid || props.isLoading}
                        btnVariant="contained"
                        btnColor="primary"
                        btnIcon={SaveRounded}
                        btnName="Save"
                        showIndicator={true}
                        isProcessing={props.isLoading} />

                    <MaterialButton
                        isDisabled={false}
                        btnVariant="contained"
                        btnColor="default"
                        btnIcon={CancelRounded}
                        btnName="Cancel"
                        onButtonClicked={props.onCancelForm} />
                </div>
            </div>
        </div>
    )
}

export default ProductCockpit;