import React from 'react';
import PropTypes from 'prop-types';
import { TextField, MenuItem, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
    Input: {
        width: '100%'
    }
}))
const Input = (props) => {
    const classes = useStyles();
    // console.log('[Input.js] ', props)

    let inputElement = null;
    switch (props.elementType) {
        case 'input':
            inputElement =
                <TextField
                    className={classes.Input}
                    id={props.id}
                    error={!props.isValid && props.isTouched}
                    label={props.label}
                    value={props.value}
                    type={props.elementConfig.type}
                    helperText={props.text}
                    margin="dense"
                    variant={props.textFieldVariant ? props.textFieldVariant : "standard"}
                    onChange={props.onInputChanged} />
            break;

        case 'select':
            inputElement =
                <TextField
                    className={classes.Input}
                    select
                    id={props.id}
                    error={!props.isValid && props.isTouched}
                    label={props.label}
                    value={props.value}
                    helperText={props.text}
                    onChange={props.onInputChanged}
                    margin="dense" >
                    {props.elementConfig.options.map(option => (
                        <MenuItem key={option.value} value={option.value}>
                            {option.label}
                        </MenuItem>
                    ))}
                </TextField>
            break;

        // case('textarea'): 
        //     inputElement = <textarea 
        //         className={inputClasses.join(' ')} 
        //         {...props.config}
        //         value={props.value}
        //         onChange={props.onElementChanged} />
        //     break;

        default:
            inputElement =
                <TextField
                    id={props.id}
                    label={props.label}
                    value={props.value}
                    type={props.type}
                    margin="normal" />
    }

    return (
        <React.Fragment>
           
                {inputElement}
                <div style={{ textAlign: 'left', color: 'red' }}>
                    {props.errorMessage}
                </div>
            
        </React.Fragment>
    )
}

Input.propTypes = {
    label: PropTypes.string,
    value: PropTypes.string,
    elementType: PropTypes.string,
    elementConfig: PropTypes.object,
    helperText: PropTypes.string,
    onInputChanged: PropTypes.func,
    validationRules: PropTypes.object,
    isTouched: PropTypes.bool,
    isValid: PropTypes.bool
}

export default Input;