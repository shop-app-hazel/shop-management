import React from 'react'
import { Snackbar, SnackbarContent, IconButton, makeStyles } from '@material-ui/core'
import { amber, green, red } from '@material-ui/core/colors';
import { CheckCircle, Warning, Error, Info, Close } from '@material-ui/icons'


const variantIcon = {
    success: CheckCircle,
    warning: Warning,
    error: Error,
    info: Info,
};

const useStyle = makeStyles(theme => ({
    success: {
        backgroundColor: green[600],
    },
    error: {
        backgroundColor: red[600],
    },
    info: {
        backgroundColor: theme.palette.primary.main,
    },
    warning: {
        backgroundColor: amber[700],
    },
    icon: {
        fontSize: 20,
    },
    iconVariant: {
        opacity: 0.9,
        marginRight: theme.spacing(1),
    },
    message: {
        display: 'flex',
        alignItems: 'center',
    },
}));

const MaterialSnackbar = (props) => {
    const classes = useStyle()
    const { vertical, horizontal, snackbarState, variant, message } = props;
    // console.log('[MaterialSnackbar.js] ', props)

    const Icon = variantIcon[variant];

    return (
        <Snackbar
            anchorOrigin={{ vertical, horizontal }}
            key={`${vertical},${horizontal}`}
            open={snackbarState}
            onClose={props.onSnackbarClose}
            autoHideDuration={5000}
        >
            <SnackbarContent
                className={classes[variant]}
                message={
                    <span id="message-id" className={classes.message}>
                        <Icon className={[classes.icon, classes.iconVariant].join(" ")} />
                        {message}
                    </span>
                }
                action={[
                    <IconButton color="inherit" onClick={props.onSnackbarClose}>
                        <Close className={classes.icon}/>
                    </IconButton>,
                ]}
            />
        </Snackbar>
    )
}

export default MaterialSnackbar;