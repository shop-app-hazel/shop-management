import React from 'react';
import {
    FormControl,
    Input,
    InputLabel,
    InputAdornment, TextField, MenuItem,
    makeStyles
} from '@material-ui/core';


const useStyles = makeStyles(theme => ({
    FormControl: {
        width: '100%'
    },
}));

const MaterialFormControl = props => {
    const classes = useStyles();

    let renderInput = null;
    switch (props.elementType) {
        case 'input':
            renderInput =
                <FormControl className={classes.FormControl}>
                    <InputLabel htmlFor={props.id}>{props.label}</InputLabel>
                    <Input
                        id={props.id}
                        error={!props.isValid && props.isTouched}
                        value={props.value}
                        type={props.elementConfig.type}
                        onChange={props.onInputChanged}
                        startAdornment={props.startAdornment
                            ? <InputAdornment position="start">{props.startAdornment.props.children}</InputAdornment>
                            : null}
                        endAdornment={props.endAdornment
                            ? <InputAdornment position="end">{props.endAdornment.props.children}</InputAdornment>
                            : null}
                    />
                </FormControl>
            break;

        case 'select':
            renderInput =
                <FormControl className={classes.FormControl}>
                    <TextField
                        select
                        label={props.label}
                        id={props.id}
                        error={!props.isValid && props.isTouched}
                        value={props.value}
                        helperText={props.text}
                        onChange={props.onInputChanged}
                        InputProps={{
                            startAdornment: props.startAdornment
                                ? <InputAdornment position="start">{props.startAdornment.props.children}</InputAdornment>
                                : null,
                            endAdornment: props.endAdornment
                                ? <InputAdornment position="end">{props.endAdornment.props.children}</InputAdornment>
                                : null,
                        }} >
                        {props.elementConfig.options.map(option => (
                            <MenuItem key={option.value} value={option.value}>
                                {option.label}
                            </MenuItem>
                        ))}
                    </TextField>
                </FormControl>
            break;

        default: renderInput = null;
    }

    // console.log('[materialFormControl js] ', props)
    // console.log(props.startAdornment)
    // console.log(props.endAdornment)

    return (
        <React.Fragment>
            {renderInput}

            <span>{props.errorMessage}</span>
        </React.Fragment>
    )
}

export default MaterialFormControl;