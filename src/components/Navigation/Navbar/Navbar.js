import React from 'react';
import { NavLink } from 'react-router-dom';
import classes from './Navbar.module.css';
import { Menu, ExitToApp } from '@material-ui/icons'
import { AppBar, Toolbar, IconButton } from '@material-ui/core';

class Navbar extends React.Component {

    menuIconHandler = () => {
        this.props.menuIconClicked()
    }

    render() {
        // console.log('[Navbar.js] ', this.props)
        return (
            <React.Fragment>
                <AppBar position="static">
                    <Toolbar className={classes.Toolbar}>
                        <div className={classes.MenuIcon}
                            onClick={this.menuIconHandler}>
                            <IconButton>
                                <Menu />
                            </IconButton>
                        </div>

                        <div className={classes.Spacer}></div>

                        <NavLink exact to="/logout">
                            <IconButton>
                                <ExitToApp />
                            </IconButton>
                        </NavLink>
                    </Toolbar>
                </AppBar>
            </React.Fragment>
        )
    }
}

export default Navbar;
