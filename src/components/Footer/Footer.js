import React from 'react'
import classes from './Footer.module.css';

const Footer = () => {
    return (
        <div className={classes.Footer}>
            hazel © 2019. All rights reserved
        </div>
    )
}

export default Footer;