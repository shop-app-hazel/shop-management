import React from 'react';
import { withRouter } from 'react-router-dom';
import classes from './Logo.module.css';
import shopLogo from '../../assets/images/logo.jpg';

const Logo = (props) => {
    const imageClickHandler = () => {
        props.history.push('/');
    }
  
    return( 
        <div className={classes.Logo} onClick={imageClickHandler}>
            <img src={shopLogo} alt="shop" />
        </div>
    )
}

export default withRouter(Logo);