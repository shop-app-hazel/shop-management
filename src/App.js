import React, { Component, Suspense } from 'react';
import { connect } from 'react-redux';
import * as actions from './store/actions/index';
import { Switch, Route, Redirect } from 'react-router-dom';
import './App.css';
import Layout from './containers/Layout/Layout';
import Dashboard from './containers/Dashboard/Dashboard';
import Spinner from './components/UI/Spinner/Spinner';
import Order from './containers/Management/manageOrders/ManageOrder';
import Users from './containers/Management/manageUser/ManageUser';
import Auth from './containers/Auth/Auth';
import Logout from './containers/Auth/Logout/Logout';

const ManageProduct = React.lazy(() => {
  return import('./containers/Management/manageProducts/ManageProducts')
})

const ProductEdit = React.lazy(() => {
  return import('./components/ManageProduct/ProductEdit/ProductEdit')
})

const ProductCreate = React.lazy(() => {
  return import('./components/ManageProduct/ProductCreate/ProductCreate')
})

class App extends Component {
  componentDidMount() {
    this.props.onAttemptAuth()
  }

  render() {

    let routes = <Auth />

    if (this.props.isAuthenticated) {
      routes = (
        <Switch>
          <Route path='/admin/users' component={Users} />
          <Route path='/admin/orders' component={Order} />
          <Route path='/admin/products/new'
            render={(props) => (
              <ProductCreate {...props} />
            )} />
          <Route path='/admin/products/:id'
            render={(props) => (
              <ProductEdit {...props} />
            )} />
          <Route path='/admin/products'
            render={(props) => (
              <ManageProduct {...props} />
            )} />
          <Route path='/logout' component={Logout} />
          <Route path='/dashboard' component={Dashboard} />
          <Redirect from="/" to="/dashboard"/>
        </Switch>
      )
    }


    return (
      <React.Fragment>
        <Layout>
          <Suspense fallback={<Spinner />}>
            {routes}
          </Suspense>
        </Layout>
      </React.Fragment>
    )
  }
}


const mapStateToProps = state => {
  return {
    isAuthenticated: state.auth.token != null
  }
}

const mapDispatchToProps = dispatch => {
  return {
    onAttemptAuth: () => dispatch(actions.authCheckState())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(App);
