import React, { Component } from 'react'
import { ArrowDropDown } from '@material-ui/icons';
import MenuButton from '../../../components/UI/MenuButton/MenuButton';
import classes from './TablePaging.module.css';
import { pageItems } from '../../../shared/selection';


class TablePaging extends Component {

    render() {
        return (
            <div className={classes.PagingWrapper}>
                <span>Rows per page: </span>
                <div className={classes.Paging}>
                    <span className={classes.PagingValue}>
                        {this.props.rowsPerPage}
                    </span>
                    <div className={classes.PagingSelection}>
                        <MenuButton
                            id='pageSelectionMenu'
                            buttonIconType={ArrowDropDown}
                            menuItems={pageItems}
                            itemClicked={(i) => { this.props.onChangeRowsPerPage(i) }} />
                    </div>
                </div>
            </div>
        )
    }
}

export default TablePaging;