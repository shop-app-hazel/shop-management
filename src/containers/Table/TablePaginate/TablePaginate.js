import React, { Component } from 'react'
import { Grid, ButtonGroup, Button } from '@material-ui/core'
import { ArrowLeftOutlined, ArrowRightOutlined } from '@material-ui/icons'

class TablePaginate extends Component {

    render() {
        // console.log(this.props)

        const from = (this.props.currentPage - 1) * this.props.rowsPerPage + 1;
        const to = (this.props.currentPage * this.props.rowsPerPage) > this.props.totalCount
            ? this.props.totalCount
            : this.props.currentPage * this.props.rowsPerPage;

        return (
            <div style={{marginTop: '10px'}}>
                <Grid container spacing={1} >
                    <Grid item xs={12} md={6}>
                        Showing {from} - {to} of {this.props.totalCount} entries
                </Grid>
                    <Grid item xs={12} md={6} style={{ display: 'flex', justifyContent: 'flex-end' }}>

                        <ButtonGroup color="primary" size="small">
                            <Button
                                disabled={this.props.currentPage === 1}
                                onClick={() => this.props.onPageChange(this.props.currentPage - 1)}>
                                <ArrowLeftOutlined />
                            </Button>
                            {this.props.pages.map(page => (
                                <Button
                                    variant={this.props.currentPage === page ? 'contained' : 'outlined'}
                                    key={page}
                                    onClick={() => this.props.onPageChange(page)}>
                                    {page}
                                </Button>
                            ))}
                            <Button
                                disabled={this.props.currentPage === this.props.pages.length}
                                onClick={() => this.props.onPageChange(this.props.currentPage + 1)}>
                                <ArrowRightOutlined />
                            </Button>
                        </ButtonGroup>
                    </Grid>
                </Grid>
            </div>

        )
    }
}

export default TablePaginate;