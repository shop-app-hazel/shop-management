import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withRouter } from 'react-router-dom'
import * as actions from '../../store/actions/index'
import classes from './Auth.module.css'
import MaterialButton from '../../components/UI/MaterialButton/MaterialButton'
import Input from '../../components/UI/Input/Input'
import { checkValidation } from '../../shared/utility';
import { Grid } from '@material-ui/core'
import Spinner from '../../components/UI/Spinner/Spinner'

import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';

class Auth extends Component {
    state = {
        isModalOpen: true,
        error: null,
        resetPasswordEmail: "",
        pageTitle: '',
        submitButtonName: '',
        authMode: 'login',
        isFormValid: false,
        authForm: {},

        firstName: {
            label: 'First Name',
            elementType: 'input',
            helperText: '',
            value: '',
            elementConfig: {
                type: 'text',
                placeholder: ''
            },
            validationRules: {
                required: true
            },
            validation: {
                isValid: false,
                errorMessage: ''
            },
            touched: false
        },

        lastName: {
            label: 'Last Name',
            elementType: 'input',
            helperText: '',
            value: '',
            elementConfig: {
                type: 'text',
                placeholder: ''
            },
            validationRules: {
                required: true
            },
            validation: {
                isValid: false,
                errorMessage: ''
            },
            touched: false
        },

        email: {
            label: 'Email Address',
            elementType: 'input',
            helperText: '',
            value: '',
            elementConfig: {
                type: 'text',
                placeholder: ''
            },
            validationRules: {
                required: true,
                isEmail: true
            },
            validation: {
                isValid: false,
                errorMessage: ''
            },
            touched: false
        },

        password: {
            label: 'Password',
            elementType: 'input',
            helperText: '',
            value: '',
            elementConfig: {
                type: 'password',
                placeholder: ''
            },
            validationRules: {
                required: true,
                minLength: 6
            },
            validation: {
                isValid: false,
                errorMessage: ''
            },
            touched: false
        }
    }

    componentDidMount() {
        this.setState({
            authForm: {
                email: this.state.email,
                password: this.state.password
            }
        })
        this.setState({ submitButtonName: 'Login' })
        this.setState({ pageTitle: 'LOGIN TO YOUR ACCOUNT' })
    }

    changePageHandler = (actionType) => {
        this.setState({ error: null })
        this.setState({ authForm: null })

        switch (actionType) {
            case 'login':
                this.setState({
                    authForm: {
                        email: this.state.email,
                        password: this.state.password
                    }
                })
                this.setState({ authMode: 'login' })
                this.setState({ pageTitle: 'LOGIN TO YOUR ACCOUNT' })
                this.setState({ submitButtonName: 'Login' })
                break;

            case 'register':
                this.setState({
                    authForm: {
                        firstName: this.state.firstName,
                        lastName: this.state.lastName,
                        email: this.state.email,
                        password: this.state.password
                    }
                })
                this.setState({ authMode: 'register' })
                this.setState({ pageTitle: 'REGISTER' })
                this.setState({ submitButtonName: 'Register' })
                break;

            case 'forgotPwd':
                this.setState({
                    authForm: {
                        email: this.state.email,
                    }
                })
                this.setState({ authMode: 'forgotPassword' })
                this.setState({ pageTitle: 'FORGOT PASSWORD' })
                this.setState({ submitButtonName: 'Reset Password' })
                break;

            default: break;
        }
    }

    inputChangedHandler = (e, formId) => {
        const updatedForm = {
            ...this.state.authForm,
            [formId]: {
                ...this.state.authForm[formId],
                value: e.target.value,
                validation: checkValidation(e.target.value, this.state.authForm[formId].validationRules),
                touched: true
            }
        }

        let isFormValid = true;
        for (let element in updatedForm) {
            isFormValid = updatedForm[element].validation.isValid && isFormValid;
        }
        this.setState({ authForm: updatedForm, isFormValid })

        // console.log('auth form value ', this.state.authForm)
    }

    submitAuthHandler = async (e) => {
        e.preventDefault();

        const formData = {};
        for (let formElement in this.state.authForm) {
            formData[formElement] = this.state.authForm[formElement].value;
        }

        switch (this.state.authMode) {
            case 'login':
                await this.props.onLogin(formData);
                if (!this.props.error) {
                    this.setState({ error: null })
                } else {
                    this.setState({ error: this.props.error })
                }
                break;

            case 'register':
                await this.props.onRegister(formData);
                if (!this.props.error) {
                    this.setState({ error: null })
                    this.props.history.push('/')
                } else {
                    this.setState({ error: this.props.error })
                }
                break;

            case 'forgotPwd':
                //TODO: submit forgotten password email
                alert('function to be implemented')
                break;
        }
    }

    render() {
        const formElementsArray = [];
        for (let key in this.state.authForm) {
            formElementsArray.push({
                id: key,
                config: this.state.authForm[key]
            });
        }

        const loginSubtitle = (
            <div className={classes.Subtitle}>
                New customer?&nbsp;
                <a onClick={() => this.changePageHandler('register')}>
                    Please register
                </a>
            </div>
        )

        const registerSubtitle = (
            <div className={classes.Subtitle}>
                Already have an login account?&nbsp;
                <a onClick={() => this.changePageHandler('login')}>
                    Login Here
                </a>
            </div>
        )

        const forgotPasswordSubtitle = (
            <div>
                <p>
                    Enter the email address you use to login
                    and we will send you a link to reset your password
                </p>

                <div className={classes.Subtitle}>
                    Already have an login account?&nbsp;
                    <a onClick={() => this.changePageHandler('login')}>
                        Login Here
                    </a>
                </div>
            </div>
        )

        return (
            <React.Fragment>
                {!this.props.isLoading ? null :
                    <Modal
                        className={classes.modal}
                        open={this.state.isModalOpen}
                        closeAfterTransition
                        BackdropComponent={Backdrop}
                        BackdropProps={{ timeout: 500 }}
                    >
                        <Fade in={this.state.isModalOpen}>
                            <Spinner />
                        </Fade>
                    </Modal>

                }

                <div className={classes.AuthFormWrapper}>
                    <div className={classes.AuthForm}>
                        {this.state.error
                            ? <h3 style={{ color: 'red' }}> {this.state.error} </h3>
                            : null
                        }
                        <h2>{this.state.pageTitle}</h2>
                        <div>
                            {this.state.authMode == 'login'
                                ? loginSubtitle
                                : this.state.authMode == 'register'
                                    ? registerSubtitle
                                    : forgotPasswordSubtitle
                            }

                            <form>
                                {formElementsArray.map(formElement => (
                                    <Grid item xs={12} key={formElement.id}>
                                        <Input
                                            id={formElement.id}
                                            label={formElement.config.label}
                                            value={formElement.config.value}
                                            helperText={formElement.config.helperText}
                                            elementType={formElement.config.elementType}
                                            elementConfig={formElement.config.elementConfig}
                                            isValid={formElement.config.validation.isValid}
                                            isTouched={formElement.config.touched}
                                            errorMessage={formElement.config.validation.errorMessage}
                                            onInputChanged={(e) => this.inputChangedHandler(e, formElement.id)}
                                        />
                                    </Grid>
                                ))}
                            </form>

                            {this.state.authMode == 'login'
                                ? <div className={classes.ForgotPassword}>
                                    <a onClick={() => this.changePageHandler('forgotPwd')}>
                                        Forgot Password?
                                        </a>
                                </div>
                                : null
                            }
                            <div className={classes.SubmitButton}>
                                <MaterialButton
                                    btnVariant="contained"
                                    btnColor="primary"
                                    btnName={this.state.submitButtonName}
                                    isDisabled={!this.state.isFormValid}
                                    onButtonClicked={this.submitAuthHandler} >
                                </MaterialButton>
                            </div>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        error: state.auth.error,
        isLoading: state.auth.isLoading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onLogin: (formData) => dispatch(actions.auth(formData)),
        onRegister: (formData) => dispatch(actions.signUp(formData))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Auth));