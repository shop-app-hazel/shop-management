import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import * as actions from '../../../store/actions/index';
import "../../../App.css";
// import classes from './ManageProducts.module.css';
import ProductTable from '../../../components/ManageProduct/ProductTable';
import MaterialButton from '../../../components/UI/MaterialButton/MaterialButton';
import MaterialSnackbar from '../../../components/UI/MaterialSnackbar/MaterialSnackbar';
import ProductDelete from '../../../components/ManageProduct/ProductDelete/ProductDelete'
import { AddOutlined } from '@material-ui/icons';

class ManageProducts extends Component {

    state = {
        error: null,
        isDialogOpen: false,
        selectedProduct: {},
        isSnackbarOpen: false
    };

    async componentDidMount() {
        this.props.onInitRowsPerPage();
        await this.props.onFetchProducts(this.props.rowsPerPage, this.props.currentPage);
        this.props.onUpdatePages(this.props.totalCount, this.props.rowsPerPage)
    }

    componentDidUpdate(prevProps) {
        if (prevProps.rowsPerPage !== this.props.rowsPerPage ||
            prevProps.currentPage !== this.props.currentPage) {
            this.props.onFetchProducts(this.props.rowsPerPage, this.props.currentPage);
        }
    }

    createProductHandler = () => {
        this.props.history.push('/admin/products/new');
    }

    editProductHandler = productId => {
        this.props.history.push(`${this.props.location.pathname}/${productId}`)
    }

    deleteProductHandler = (product) => {
        // console.log('[ProductTable.js] productId ', product._id);
        this.setState({ isDialogOpen: true });
        this.setState({ selectedProduct: product });
    }

    closeSnackbarHandler = () => {
        this.setState({ isSnackbarOpen: false })
    }

    onCloseDialogHandler = async (value) => {
        // console.log('[ProductTable.js] dialog value', value)

        this.setState({ isDialogOpen: false });
        if (value === 'YES') {
            await this.props.onDeleteProduct(this.state.selectedProduct._id);

            this.setState({ isSnackbarOpen: true })
            if (this.props.error) {
                this.setState({ error: this.props.error })
            } else {
                this.props.onUpdateCurrentPage(1)
                this.props.onFetchProducts(this.props.rowsPerPage, this.props.currentPage)
            }
        }
    }


    render() {
        return (
            <Fragment>
                {this.state.error
                    ? <MaterialSnackbar
                        vertical="bottom"  horizontal="left"  variant="error"
                        snackbarState={this.state.isSnackbarOpen}
                        onSnackbarClose={this.closeSnackbarHandler}
                        message={this.props.error} />
                    : <MaterialSnackbar
                        vertical="bottom" horizontal="left" variant="success"
                        snackbarState={this.state.isSnackbarOpen}
                        onSnackbarClose={this.closeSnackbarHandler}
                        message="Product deleted successfully" />
                }

                <div className="Container">
                    <div className="HeaderWrapper">
                        <span className="HeaderTitle">Products</span>
                        <MaterialButton
                            btnVariant="contained" btnColor="primary"
                            btnIcon={AddOutlined} btnName="create"
                            onButtonClicked={this.createProductHandler} />
                    </div>

                    <ProductTable 
                        onDeleteProduct={this.deleteProductHandler}
                        onEditProduct={this.editProductHandler} />

                </div>

                <ProductDelete
                    open={this.state.isDialogOpen}
                    onClose={this.onCloseDialogHandler}
                    selectedProduct={this.state.selectedProduct} />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        products: state.products.products,
        totalCount: state.products.totalCount,
        rowsPerPage: state.products.rowsPerPage,
        currentPage: state.products.currentPage,
        error: state.products.error,
        isLoading: state.products.isLoading
    }
}

const mapDispatchToProps = dispatch => {
    return {
        onInitRowsPerPage: () => dispatch(actions.initRowsPerPage()),
        onUpdatePages: (totalCnt, rowsPerPage) => dispatch(actions.updatePages(totalCnt, rowsPerPage)),
        onUpdateCurrentPage: (pageNum) => dispatch(actions.changePage(pageNum)),
        onFetchProducts: (rowsPerPage, currentPage) => dispatch(actions.fetchProducts(rowsPerPage, currentPage)),
        onDeleteProduct: (productId) => dispatch(actions.deleteProduct(productId))
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ManageProducts);