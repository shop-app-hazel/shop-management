import React, { Component } from 'react';
import { connect } from 'react-redux';
import classes from './Layout.module.css';
import Navbar from '../../components/Navigation/Navbar/Navbar';
import SideDrawer from '../SideDrawer/SideDrawer';
import Footer from '../../components/Footer/Footer';


class Layout extends Component {
    state = {
        isMenuIconClicked: false
    }

    menuIconHandler = () => {
        this.setState({ isMenuIconClicked: true })
    }

    closeDrawerHandler = () => {
        this.setState({ isMenuIconClicked: false })
    }

    render() {

        let renderLayout = (
            <div className={classes.Wrapper}>
                <div className="login-background">
                    {this.props.children}
                </div>
            </div>
        )

        if (this.props.isAuthenticated) {
            renderLayout =
                <div className={classes.Wrapper}>
                    <div className={classes.SideDrawer}>
                        <SideDrawer
                            isOpen={this.state.isMenuIconClicked}
                            onSideDrawerClosed={this.closeDrawerHandler} />
                    </div>

                    <div className={classes.RightWrapper}>
                        <Navbar menuIconClicked={this.menuIconHandler} />

                        <main className={classes.Content}>
                            {this.props.children}
                        </main>

                        <Footer />
                    </div>
                </div>
        }

        return (
            <React.Fragment>
                {renderLayout}
            </React.Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        isAuthenticated: state.auth.token != null
    }
  }

  
export default connect(mapStateToProps)(Layout);