import React from 'react'
import NavigationItems from '../../components/Navigation/NavigationItems/NavigationItems';
import { Drawer } from '@material-ui/core';
import './SideDrawer.css'
import Logo from '../../components/Logo/Logo';


const SideDrawer = props => {
   
    const closeDrawerHandler = () => event => {
        props.onSideDrawerClosed()
    };

    return (
        <React.Fragment>
            <Drawer
                variant={props.isOpen ? "temporary" : "permanent"}
                anchor="left"
                open={props.isOpen}
                onClose={closeDrawerHandler()} >
                <div className="SideDrawer">
                    <div className="Header">
                        <Logo />
                    </div>
                    <NavigationItems onNavClicked={closeDrawerHandler()}/>
                </div>
            </Drawer>
        </React.Fragment>
    )
}

export default SideDrawer;